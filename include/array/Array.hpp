#ifndef __array_hpp
#define __array_hpp

#include <type_traits>
#include <cstdint>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <initializer_list>
#include <vector>
#include <memory>

namespace array {

  template<typename dtype>
  class Array {
  public:

    class ArrayIterator : std::iterator<std::bidirectional_iterator_tag, dtype>
    {
      public:

        ArrayIterator() {}
        ArrayIterator(dtype* _buffer, int _start=0, int _by=1) : buffer(_buffer), start(_start), by(_by) {
          buffer += start;
        }
        ArrayIterator(const ArrayIterator& it) : buffer(it.buffer), start(it.start), by(it.by) {}

        ArrayIterator& operator= (const ArrayIterator& it) {
          if (&it == this) {
            return *this;
          }
          buffer = it.buffer;
          start = it.start;
          by = it.by;
          return *this;
        }

        ArrayIterator& operator+=(std::uint64_t incr) {
          buffer += incr*by;
          return *this;
        }

        ArrayIterator& operator++() {
          buffer += by;
          return *this;
        }

        ArrayIterator operator++(int) {
          const ArrayIterator tmp(*this);
          operator++();
          return tmp;
        }

        ArrayIterator& operator-=(std::uint64_t incr) {
          buffer -= incr*by;
          return *this;
        }

        ArrayIterator& operator--() {
          buffer -= by;
          return *this;
        }

        ArrayIterator operator--(int) {
          const ArrayIterator tmp(*this);
          operator--();
          return tmp;
        }

        bool operator==(const ArrayIterator& rhs) {
          return buffer==rhs.buffer;
        }
        bool operator!=(const ArrayIterator& rhs) {
          return buffer!=rhs.buffer;
        }

        dtype& operator*() { return *buffer; }

        const dtype& operator*() const { return *buffer; }

        dtype* data() { return buffer; }
        const dtype* data() const { return buffer; }

      private:
        mutable dtype* buffer;
        std::uint64_t start;
        std::uint64_t by;
    };

    Array () {
      is_ref = false;
    }

    Array (std::vector<std::uint64_t>& _shape) {
      shape = _shape;
      init();
    }

    Array (const std::vector<std::uint64_t>& _shape) {
      shape = _shape;
      init();
    }

    Array (const std::vector<std::uint64_t>& _shape, dtype* _buffer) {
      shape = _shape;
      init(_buffer);
    }


    Array (std::initializer_list<std::uint64_t> _shape) {
      shape = std::vector<std::uint64_t>(_shape.begin(), _shape.end());
      init();
    }

    Array (std::initializer_list<std::uint64_t> _shape, dtype* _buffer) {
      shape = std::vector<std::uint64_t>(_shape.begin(), _shape.end());
      init(_buffer);
    }

    ~Array () {
      // std::cerr << "Array::~Array" << std::endl;
      dealloc();
    }

    // copy constructor
    Array (const Array& arr) {
      copy(arr);
    }

    // copy assignemnt operator
    Array& operator= (const Array& arr) {
      if (&arr == this) {
        return *this;
      }
      dealloc();
      copy(arr);
      return *this;
    }

    // move constructor
    Array (Array&& arr) {
      move(arr);
    }

    // move assignment operator
    Array& operator= (Array&& arr) {
      // std::cerr << "operator=(Array&&)" << std::endl;
      if (this != &arr) {
        dealloc();
        move(arr);
      }
      return *this;
    }


    void init_from (std::initializer_list<std::uint64_t> _shape, dtype* _buffer) {
      shape = std::vector<std::uint64_t>(_shape.begin(), _shape.end());
      init(_buffer);
    }

    void init_from (const std::vector<std::uint64_t>& _shape, dtype* _buffer) {
      shape = _shape;
      init(_buffer);
    }


    template<typename T>
    std::uint64_t _get_idx(const std::uint64_t* _stride, T arg0) const {
      return *(_stride) * arg0;
    }

    template<typename T, typename... Args>
    std::uint64_t _get_idx(const std::uint64_t* _stride, T arg0, Args... args) const {
      return *(_stride) * arg0 + _get_idx(_stride + 1, args...);
    }


    template<typename... Args>
    std::uint64_t get_idx(Args... args) const {
      return _get_idx(stride.data(), args...) + offset;
    }

    template<typename... Args>
    dtype& get(Args... args) {
      return buffer[get_idx(args...)];
    }

    template<typename... Args>
    const dtype& get(Args... args) const {
      return buffer[get_idx(args...)];
    }

    template<typename... Args>
    ArrayIterator at(Args... args) {
      std::uint64_t start = get_idx(args...);
      return ArrayIterator(buffer, start, stride[ndim - 1]);
    }

    template<typename... Args>
    const ArrayIterator at(Args... args) const {
      std::uint64_t start = get_idx(args...);
      return ArrayIterator(buffer, start, stride[ndim - 1]);
    }

    void slice(Array& _slice, int idim, int idx) {
      _slice.shape = shape;
      _slice.stride = stride;
      _slice.buffer = buffer;
      _slice.is_ref = true;

      _slice.shape.erase(_slice.shape.begin() + idim);
      _slice.stride.erase(_slice.stride.begin() + idim);
      _slice._size = calc_size(_slice.shape);
      _slice.offset = offset + idx*stride[idim];
      _slice.ndim = _slice.shape.size();
    }

    std::unique_ptr<Array> slice (int idim, int idx) {
      std::unique_ptr<Array> arr = std::unique_ptr<Array>(new Array);
      slice(*arr, idim, idx);
      return arr;
    }


    ArrayIterator begin() const {
      return ArrayIterator(buffer, offset, stride[ndim - 1]);
    }

    ArrayIterator rbegin() const {
      std::uint64_t end_point = offset + ((size() - 1) * stride[ndim-1]);
      return ArrayIterator(buffer, end_point, -stride[ndim-1]);
    }

    ArrayIterator end() const {
      std::uint64_t end_point = offset + (size() * stride[ndim-1]);
      return ArrayIterator(buffer, end_point, stride[ndim-1]);
    }

    ArrayIterator rend() const {
      return ArrayIterator(buffer, offset-stride[ndim - 1], -stride[ndim - 1]);
    }

    const dtype& operator[](unsigned idx) const { return buffer[idx]; }
    dtype& operator[](unsigned idx) { return buffer[idx]; }

    // Alias for get_buffer
    const dtype* data() const { return buffer; }
    dtype* data() { return buffer; }

    const dtype* get_buffer() const { return buffer; }
    dtype* get_buffer() { return buffer; }

    std::uint64_t size() const { return _size; }

    unsigned get_ndim() const { return ndim; }

    std::vector<std::uint64_t> get_shape() const { return shape; }

    bool get_is_ref() const { return is_ref; }

  private:
     // The underlaying buffer that the object wraps
     dtype* buffer = nullptr;

     // shape of array
     std::vector<std::uint64_t> shape;

     // strides of array
     std::vector<std::uint64_t> stride;

     // initial offset to buffer
     std::uint64_t offset;

     // total number of elements in array
     std::uint64_t _size;

     // number of dimensions
     unsigned ndim;

     bool is_ref;

     unsigned calc_size(const std::vector<std::uint64_t>& _shape) {
       std::uint64_t res = 1;
       for (unsigned idx=0; idx<_shape.size(); idx++) {
         res *= _shape[idx];
       }
       return res;
     }

     void calc_stride(const std::vector<std::uint64_t>& _shape, std::vector<std::uint64_t>& _stride) {
       unsigned size = _shape.size();
       _stride.resize(size);
       _stride[size - 1] = 1;
       for (unsigned i=size-1; i>0; i--) {
         _stride[i-1] = _stride[i] * _shape[i];
       }
     }

     void dealloc() {
       // std::cerr << "dealloc: is_ref=" << is_ref << ", buffer=" << buffer << std::endl;
       if (! is_ref && buffer != nullptr) {
         delete[] buffer;
       }
     }

     void copy(const Array& arr) {
       // std::cerr << "copy(const Array& arr)" << std::endl;
       offset = 0;
       shape = arr.shape;
       calc_stride(shape, stride);
       _size = calc_size(shape);
       ndim = arr.ndim;
       buffer = new dtype[_size]();
       auto this_it = begin();
       for (auto it=arr.begin(); it!=arr.end(); it++) {
         *this_it = *it;
         this_it++;
       }
       is_ref = false;
     }

     void move(Array& arr) {
       shape = arr.shape;
       calc_stride(shape, stride);
       _size = calc_size(shape);
       ndim = arr.ndim;
       buffer = arr.buffer;
       is_ref = arr.is_ref;
       arr.buffer = nullptr;

     }

     /**
      * Helper function for initializing from shape parameter
      */
     void init () {
       // std::cerr << "init()" << std::endl;
       offset = 0;
       ndim = shape.size();
       _size = calc_size(shape);
       calc_stride(shape, stride);
       buffer = new dtype[_size]();
       is_ref = false;
     }

     /**
      * Helper function for initializing from shape and buffer parameters
      */
     void init (dtype* _buffer) {
       // std::cerr << "init(dtype*)" << std::endl;
       offset = 0;
       ndim = shape.size();
       _size = calc_size(shape);
       calc_stride(shape, stride);
       buffer = _buffer;
       is_ref = true;
     }

  };
}

#endif
