cmake_minimum_required(VERSION 3.13)
set(CMAKE_CXX_STANDARD 11)

file(STRINGS version.txt array_VERSION)

project(array VERSION ${array_VERSION} LANGUAGES CXX)

set(INCLUDE_DIR include)
set(TEST_DIR test)
set(BENCH_DIR bench)

include_directories(${INCLUDE_DIR})

set(TEST_SOURCES ${TEST_DIR}/test_array.cpp ${TEST_DIR}/test_main.cpp)
set(BENCH_SOURCES ${BENCH_DIR}/bench.cpp)


add_executable(test_main ${TEST_SOURCES})
target_compile_options(test_main PUBLIC -g)


add_executable(bench ${BENCH_SOURCES})
target_compile_options(bench PUBLIC -O3)
