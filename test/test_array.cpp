#include <iostream>
#include <vector>

#include "array/Array.hpp"

#include "catch.hpp"


TEMPLATE_TEST_CASE (
  "Array constructors, destructor and copy/move operators works as expected",
  "[Array][constructor][destructor]",
  int, unsigned, float, double
)
{
  unsigned test_size = 10;

  SECTION("constructor/destructor work when initializing without buffer"){
    array::Array<TestType> arr({5, 2});
    REQUIRE(arr.get_is_ref() == false);
  }

  SECTION("constructor/destructor work when initializing without buffer"){
    array::Array<TestType> arr({5, 2});
    REQUIRE(arr.get_is_ref() == false);
  }


  SECTION("constructor/destructor work when initializing with buffer"){
    std::vector<TestType> vec(10);
    array::Array<TestType> arr({5, 2}, vec.data());
    REQUIRE(arr.get_is_ref() == true);
  }

}

TEMPLATE_TEST_CASE(
  "Array copy constructor and assignment operator work",
  "[Array][copy]",
  int, unsigned, float, double
)
{
  unsigned test_size = 10;

  SECTION("copy constructor works") {
    std::vector<TestType> vec(10, 1);
    array::Array<TestType> arr({5, 2}, vec.data());
    array::Array<TestType> arr_copy(arr);
    // array::Array<TestType> arr_copy = arr;
    REQUIRE(arr.get_is_ref() == true);
    REQUIRE(arr_copy.get_is_ref() == false);
    REQUIRE(arr.data() != arr_copy.data());

    unsigned nclose = 0;
    for (unsigned idx=0; idx<arr.size(); idx++) {
      if (arr.data()[idx] == arr_copy.data()[idx]) {
        nclose++;
      }
    }
    REQUIRE(nclose == test_size);
  }

  SECTION("copy assignment works") {
    std::vector<TestType> vec(10, 1);
    array::Array<TestType> arr({5, 2}, vec.data());
    array::Array<TestType> arr_copy({5, 2});
    arr = arr_copy;

    REQUIRE(arr.data() != arr_copy.data());

    unsigned nclose = 0;
    for (unsigned idx=0; idx<arr.size(); idx++) {
      if (arr.data()[idx] == arr_copy.data()[idx]) {
        nclose++;
      }
    }
    REQUIRE(nclose == test_size);
  }
}

TEMPLATE_TEST_CASE(
  "Array move constructor and assignment operator work",
  "[Array][move]",
  int, unsigned, float, double
)
{
  SECTION("move constructor works") {
    array::Array<TestType> arr(array::Array<TestType>({5, 2}));
    array::Array<TestType> arr1 = array::Array<TestType>({5, 2});
  }

  SECTION("move assignment works when rhs is ref") {
    std::vector<TestType> vec(10, 1);
    array::Array<TestType> arr;
    arr = array::Array<TestType>({5, 2}, vec.data());
  }

  SECTION("move assignment works when rhs isn't ref") {
    array::Array<TestType> arr;
    arr = array::Array<TestType>({5, 2});
  }

}


TEMPLATE_TEST_CASE(
  "Array access operators works as expected",
  "[Array][operator[]][at][get]",
  int, double, float, unsigned
)
{
  std::vector<TestType> vec(40);
  for (unsigned idx=0; idx<vec.size(); idx++) {
    vec[idx] = static_cast<TestType>(idx);
  }

  array::Array<TestType> arr({4, 5, 2}, vec.data());
  array::Array<TestType> arr_1d({40}, vec.data());
  const array::Array<TestType> arr_const({4, 5, 2}, vec.data());

  SECTION ("operator[] works") {
    arr[0] = 1;
    REQUIRE(vec[0] == 1);
  }

  SECTION ("get works") {
    REQUIRE(arr.get(1, 2, 1) == 15);
    REQUIRE(arr_const.get(1, 2, 1) == 15);

    arr.get(1, 2, 0) = 0;
    REQUIRE(arr.get(1, 2, 0) == 0);

  }

  SECTION ("at works") {
    auto iter = arr.at(1, 2, 1);
    REQUIRE(*iter == 15);
    iter = arr_1d.at(15);
    REQUIRE(*iter == 15);
  }

  SECTION ("const at works") {
    auto val = arr_const.at(1, 2, 1);
    REQUIRE(*val == 15);
    val++;
    REQUIRE(*val == 16);
  }
}


TEMPLATE_TEST_CASE(
  "Array slicing works",
  "[Array][slice]",
  int, float, double, unsigned
)
{
  array::Array<TestType> arr({4, 5, 2});
  for (unsigned idx=0; idx<arr.size(); idx++) {
    arr[idx] = static_cast<TestType>(idx);
  }


  SECTION ("slice as object") {
    array::Array<TestType> slice;
    arr.slice(slice, 0, 1);
    REQUIRE(slice.size() == 10);
    REQUIRE(slice.get_shape()[0] == 5);
    REQUIRE(slice.get_shape()[1] == 2);
    REQUIRE(slice.get_ndim() == 2);
    REQUIRE(slice.get_is_ref() == true);
  }

  SECTION ("slice as pointer") {
    auto slice = arr.slice(0, 1);
    REQUIRE(slice->size() == 10);
    REQUIRE(slice->get_shape()[0] == 5);
    REQUIRE(slice->get_shape()[1] == 2);
    REQUIRE(slice->get_ndim() == 2);
    REQUIRE(slice->get_is_ref() == true);
  }

  SECTION ("copying from slice works") {
    array::Array<TestType> slice;
    arr.slice(slice, 0, 1);
    array::Array<TestType> copy;
    copy = slice;
    REQUIRE(copy.get_is_ref() == false);
  }

  SECTION ("copying from pointer to slice works") {
    array::Array<TestType> slice = *(arr.slice(0, 1));
    REQUIRE(slice.get_is_ref() == false);
  }

}



TEMPLATE_TEST_CASE(
  "ArrayIterator works",
  "[ArrayIterator]",
  int, double, float, unsigned
)
{
  array::Array<TestType> arr({4, 5, 2});
  for (unsigned idx=0; idx<arr.size(); idx++) {
    arr[idx] = static_cast<TestType>(idx);
  }

  SECTION ("basic iterator works as expected") {
    TestType val = 0;
    std::uint64_t nclose = 0;
    for (auto it=arr.begin(); it != arr.end(); it++) {
      if (*it == val) {
        nclose++;
      }
      val++;
    }
    REQUIRE(nclose == arr.size());
  }

  SECTION ("iterator works with slices") {
    TestType val = 10;
    std::uint64_t nclose = 0;
    array::Array<TestType> slice;
    arr.slice(slice, 0, 1);
    for (auto it=slice.begin(); it != slice.end(); it++) {
      if (*it == val) {
        nclose++;
      }
      val++;
    }
    REQUIRE(nclose == slice.size());
  }

  SECTION ("iterator works with pointers to slices") {
    TestType val = 10;
    std::uint64_t nclose = 0;
    auto slice = arr.slice(0, 1);
    for (auto it=slice->begin(); it != slice->end(); it++) {
      if (*it == val) {
        nclose++;
      }
      val++;
    }
    REQUIRE(nclose == slice->size());
  }

  SECTION ("ArrayIterator works in backwards direction")
  {
    TestType val = 19;
    std::uint64_t nclose = 0;
    auto slice = arr.slice(0, 1);
    auto it = slice->end();
    for (auto it=slice->rbegin(); it != slice->rend(); it++) {
      if (*it == val) {
        nclose++;
      }
      val--;
    }
    REQUIRE(nclose == slice->size());
  }

  SECTION ("ArrayIterator works when jumping values")
  {
    auto iter = arr.begin();
    REQUIRE(*iter == 0);
    iter+=2;
    REQUIRE(*iter == 2);
  }

}
