#include <chrono>
#include <iostream>
#include <vector>

#include "array/Array.hpp"

#include "cxxopts.hpp"


using duration_ms = std::chrono::duration<double, std::milli>;
using time_point = std::chrono::time_point<std::chrono::high_resolution_clock>;

inline std::chrono::time_point<std::chrono::high_resolution_clock> now () {
  return std::chrono::high_resolution_clock::now();
}


template<typename T>
void add (
  T iter0,
  T iter1,
  T result,
  T result_end
)
{
  for (; result!=result_end; ++result) {
    *result = *iter0 + *iter1;
    ++iter0;
    ++iter1;
  }
}



int main (int argc, char** argv) {

  cxxopts::Options options(
       "benchmark",
       "array::Array benchmark");
  options.add_options()
    ("h,help", "Help")
    ("n,niter", "Number of iteration", cxxopts::value<unsigned>()->default_value("1000"))
    ("s,size", "Array size", cxxopts::value<unsigned>()->default_value("1000000"))
  ;
  auto parsed = options.parse(argc, argv);
  if (parsed["help"].as<bool>()) {
    std::cerr << options.help() << std::endl;
    return 0;
  }

  unsigned size = parsed["size"].as<unsigned>();
  unsigned niter = parsed["niter"].as<unsigned>();

  array::Array<double> arr0({size, 2});
  array::Array<double> arr1({size, 2});
  array::Array<double> result({size, 2});

  double* arr0_ptr = new double[size];
  double* arr1_ptr = new double[size];
  double* result_ptr = new double[size];

  for (unsigned idx=0; idx<size; idx++) {
    arr0.get(idx, 0) = static_cast<double>(idx);
    arr1.get(idx, 0) = static_cast<double>(idx);
    arr0.get(idx, 1) = static_cast<double>(idx);
    arr1.get(idx, 1) = static_cast<double>(idx);
    arr0_ptr[idx] = static_cast<double>(idx);
    arr1_ptr[idx] = static_cast<double>(idx);
  }

  time_point t0 = now();
  for (unsigned iter=0; iter<niter; ++iter) {
    add(arr0_ptr, arr1_ptr, result_ptr, result_ptr + size);
  }
  duration_ms delta_ptr = now() - t0;
  std::cerr << "Pointer add took " << delta_ptr.count() << " ms" << std::endl;
  std::cerr << "   " << delta_ptr.count()/niter << " ms per loop" << std::endl;

  auto slice0 = *(arr0.slice(1, 0));
  auto slice1 = *(arr1.slice(1, 0));
  auto result_slice = *(result.slice(1, 0));

  t0 = now();
  for (unsigned iter=0; iter<niter; ++iter) {
    add(slice0.begin(), slice1.begin(), result_slice.begin(), result_slice.end());
  }
  duration_ms delta_array = now() - t0;

  std::cerr << "Array add took " << delta_array.count() << " ms" << std::endl;
  std::cerr << "   " << delta_array.count()/niter << " ms per loop" << std::endl;

  std::cerr << "Pointer add " << delta_array.count() / delta_ptr.count() << " times faster" << std::endl;

  delete [] arr0_ptr;
  delete [] arr1_ptr;
  delete [] result_ptr;


}
